package edu.boisestate.mvail.photogallery;

import android.support.v4.app.Fragment;

public class PhotoGalleryActivity extends SingleFragmentActivity {

    protected Fragment createFragment() {
        return PhotoGalleryFragment.newInstance();
    }
}
