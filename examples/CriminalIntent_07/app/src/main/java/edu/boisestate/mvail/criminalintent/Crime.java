package edu.boisestate.mvail.criminalintent;

import java.util.UUID;

/**
 * Created by Mason on 8/15/2015.
 */
public class Crime {
    private UUID mId; //"Universal Unique Identifier"
    private String mTitle;

    public Crime() {
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }
}
