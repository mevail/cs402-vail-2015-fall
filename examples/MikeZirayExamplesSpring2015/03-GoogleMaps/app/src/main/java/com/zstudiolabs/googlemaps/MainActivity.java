package com.zstudiolabs.googlemaps;

import android.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Map;


public class MainActivity extends ActionBarActivity {

    public static final String IS_FAVORTE = "isFavorte";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Boolean isFavorite = true;
        if( savedInstanceState != null )
        {
            isFavorite = savedInstanceState.getBoolean(IS_FAVORTE);
//            CheckBox favoriteCB = (CheckBox)findViewById(R.id.checkbox);
//            favoriteCB.setChecked(isFavorite);
            Log.i("TAG", "favorite: " + isFavorite);
        }

        MapFragment mapFragment = (MapFragment)(getFragmentManager().findFragmentById(R.id.map));

        GoogleMap googleMap = mapFragment.getMap();

        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(43.59917686, -116.1985597);
        markerOptions.position(latLng);
        markerOptions.title("Micron Engineering Center");
        markerOptions.snippet("CS 402");

        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.bronco));

        googleMap.addMarker(markerOptions);

        googleMap.setMyLocationEnabled(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Boolean isFavorite = false;
        outState.putBoolean( IS_FAVORTE, isFavorite);

        Log.i("TAG", "Saved instance state");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
