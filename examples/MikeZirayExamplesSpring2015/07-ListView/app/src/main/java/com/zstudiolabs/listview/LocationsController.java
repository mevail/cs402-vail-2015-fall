package com.zstudiolabs.listview;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 3/10/15.
 */
public class LocationsController {

    private Context mAppContext;
    private static LocationsController sharedLocationsController;

    private ArrayList<LocationsModel> mLocationsModels;
    private LocationsController( Context appContext )
    {
        mAppContext = appContext;
    }

    public static LocationsController sharedInstance( Context applicationContext )
    {
        if( sharedLocationsController == null ){
            sharedLocationsController = new LocationsController( applicationContext.getApplicationContext() );
            sharedLocationsController.mLocationsModels = new ArrayList<LocationsModel>();
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 1", "Description 1", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 2", "Description 2", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 3", "Description 3", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 4", "Description 4", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 5", "Description 5", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 6", "Description 6", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 7", "Description 7", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 8", "Description 8", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 9", "Description 9", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 10", "Description 10", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 11", "Description 11", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 12", "Description 12", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 13", "Description 13", new LatLng(123.123, 123.132)));
            sharedLocationsController.mLocationsModels.add(new LocationsModel("Title 14", "Description 14", new LatLng(123.123, 123.132)));
        }
        return sharedLocationsController;
    }

    public ArrayList<LocationsModel> getLocationsModels() {
        return mLocationsModels;
    }

    public void setLocationsModels(ArrayList<LocationsModel> locationsModels) {
        mLocationsModels = locationsModels;
    }
}
