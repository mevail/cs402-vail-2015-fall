package com.zstudiolabs.listview;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by michael.ziray on 3/10/15.
 */
public class LocationsModel {

    private String mLocationName;
    private String mLocationDescription;
    private LatLng mLocationCoordinates;

    public LocationsModel()
    {
    }

    public LocationsModel(String locationName, String locationDescription, LatLng locationCoordinates) {
        mLocationName = locationName;
        mLocationDescription = locationDescription;
        mLocationCoordinates = locationCoordinates;
    }

    public LatLng getLocationCoordinates() {
        return mLocationCoordinates;
    }

    public void setLocationCoordinates(LatLng locationCoordinates) {
        mLocationCoordinates = locationCoordinates;
    }

    public String getLocationDescription() {
        return mLocationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        mLocationDescription = locationDescription;
    }

    public String getLocationName() {
        return mLocationName;
    }

    public void setLocationName(String locationName) {
        mLocationName = locationName;
    }

    // Default ArrayAdapter will use this method to display a String in its TextView
//    @Override
//    public String toString() {
//        return mLocationName + " - " + mLocationDescription;
//    }
}
