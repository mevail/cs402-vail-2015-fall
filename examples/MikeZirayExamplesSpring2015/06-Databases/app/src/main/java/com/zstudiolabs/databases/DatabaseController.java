package com.zstudiolabs.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 3/3/15.
 */
public class DatabaseController extends SQLiteOpenHelper {

    /* DATABASE INFO */
    private static final String DB_NAME = "android_db";
    private static final int DB_VERSION = 1;


    public DatabaseController( Context context )
    {
        super( context, DB_NAME, null, DB_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {
        // Create Coordinates Table
        String locationsTableQuery = "CREATE TABLE " + LocationsContract.LocationsEntry.TABLE_COORDINATES+ "("+
                "_id integer primary key autoincrement, " +
                LocationsContract.LocationsEntry.COLUMN_LATITUDE  + " real, " +
                LocationsContract.LocationsEntry.COLUMN_LONGITUDE + " real " +
                ")";
        try{
            database.execSQL( locationsTableQuery );
        }
        catch ( Exception exception )
        {
            Log.e("MZ", exception.getMessage());
        }

        // Create Locations Table
        String coordinatesTableQuery = "CREATE TABLE " + LocationsContract.LocationsEntry.TABLE_LOCATIONS +
                "("+
                "_id integer primary key autoincrement, " +
                LocationsContract.LocationsEntry.COLUMN_LOCATION_NAME + " varchar(255)," +
                LocationsContract.LocationsEntry.COLUMN_LOCATION_ID + " integer references " +
                LocationsContract.LocationsEntry.TABLE_LOCATIONS + "(_id)," +
                LocationsContract.LocationsEntry.COLUMN_DESCRIPTION + " varchar(1023) " +
                ")";

        database.execSQL( coordinatesTableQuery );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        switch ( oldVersion )
        {
            case 1:
                // Adds new table Majors: db version 2
            case 2:
                // Adds new column 'times' to table Majors: db version 3
            case 3:
                // Removes 'times' from table Majors: db version 4
            default:
                // Unhandled migration
                break;
        }
    }


    public void addLocation( LocationModel newLocation )
    {
        AddLocationTask addLocationTask = new AddLocationTask();
        addLocationTask.execute(newLocation);
    }

    private void addLocationAsync( LocationModel newLocation )
    {
        ContentValues contentValues = new ContentValues();
        LatLng latLngCoordinate = newLocation.getLocationCoordinates();
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LATITUDE, latLngCoordinate.latitude );
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LONGITUDE, latLngCoordinate.longitude );

        long coordinateID = getWritableDatabase().insert( LocationsContract.LocationsEntry.TABLE_COORDINATES, null, contentValues);


        contentValues = new ContentValues();
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LOCATION_NAME, newLocation.getLocationName() );
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_DESCRIPTION,   newLocation.getLocationDescription() );
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LOCATION_ID,   coordinateID );
        long locationID = getWritableDatabase().insert( LocationsContract.LocationsEntry.TABLE_LOCATIONS, null, contentValues);
    }


    public void removeLocation( LocationModel locationToRemove )
    {
        RemoveLocationTask removeLocationTask = new RemoveLocationTask();
        removeLocationTask.execute( locationToRemove );
    }

    private void removeLocationAsync( LocationModel locationToRemove )
    {
        String [] whereArguments = { String.valueOf(locationToRemove.getLocationID()) };

        try {
            int delete = getWritableDatabase().delete(LocationsContract.LocationsEntry.TABLE_COORDINATES,
                    LocationsContract.LocationsEntry._ID + "=",
                    whereArguments);
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private class AddLocationTask extends AsyncTask<LocationModel, Void, Void>
    {
        @Override
        protected Void doInBackground( LocationModel...parameters )
        {
            for (LocationModel currentLocation : parameters) {
                addLocationAsync(currentLocation);
            }
            return null;
        }
    }


    private class RemoveLocationTask extends AsyncTask<LocationModel, Void, Void>
    {
        @Override
        protected Void doInBackground(LocationModel... parameters)
        {
            for (LocationModel currentLocation : parameters) {
                removeLocationAsync(currentLocation);
            }
            return null;
        }
    }



    public void getLocations()
    {
        ReadLocationsTask readLocationsTask = new ReadLocationsTask();
        readLocationsTask.execute();
    }

    private class ReadLocationsTask extends AsyncTask<Void, Void, ArrayList<LocationModel>>
    {
        @Override
        protected ArrayList<LocationModel> doInBackground(Void... params) {
            return readLocationsAsync();
        }

        @Override
        protected void onPostExecute(ArrayList<LocationModel> locationModels) {
            super.onPostExecute(locationModels);

            // Post to locationModels LocationsController
        }
    }

    private ArrayList<LocationModel> readLocationsAsync()
    {
        String [] columns = new String[]{LocationsContract.LocationsEntry.COLUMN_LOCATION_NAME,
                LocationsContract.LocationsEntry.COLUMN_DESCRIPTION,
                LocationsContract.LocationsEntry.COLUMN_LATITUDE,
                LocationsContract.LocationsEntry.COLUMN_LONGITUDE
        };
        String rawQueryString = "SELECT " + columns[0] +"," + columns[1] +","+ columns[2]+","+columns[3] + " FROM `" + LocationsContract.LocationsEntry.TABLE_LOCATIONS + "` INNER JOIN " + LocationsContract.LocationsEntry.TABLE_COORDINATES + " ON Locations.location_id=Coordinates._id";
        Cursor cursorResults = getReadableDatabase().rawQuery(rawQueryString, null);
//                query(false, LocationsContract.LocationsEntry.TABLE_LOCATIONS, columns, null, null, null, null, null, null );

        ArrayList<LocationModel> locationsArray = new ArrayList<LocationModel>();
        int index = 0;
        if( cursorResults.moveToFirst() ) {
            do {
                LocationModel newLocation = new LocationModel();
                newLocation.setLocationName(cursorResults.getString(0));
                newLocation.setLocationDescription(cursorResults.getString(1));
                newLocation.setLocationCoordinates(new LatLng( Double.parseDouble(cursorResults.getString(2)), Double.parseDouble(cursorResults.getString(3))));
                Log.i("TAG", index++ + ": " + cursorResults.getString(0) + ", " + cursorResults.getString(1));

                locationsArray.add(newLocation);
            } while (cursorResults.moveToNext());
        }
        return locationsArray;
    }
}