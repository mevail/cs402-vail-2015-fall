package com.zstudiolabs.databases;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by michael.ziray on 3/3/15.
 */
public class GPSCoordinate {

    private int coordinateID;
    private LatLng mLatLng;


    public int getCoordinateID() {
        return coordinateID;
    }

    public void setCoordinateID(int coordinateID) {
        this.coordinateID = coordinateID;
    }

    public double getLatitude() {
        return mLatLng.latitude;
    }

    public double getLongitude() {
        return mLatLng.longitude;
    }

    public LatLng setLatLng( LatLng newLatLong ){ return this.mLatLng = newLatLong; }
}
